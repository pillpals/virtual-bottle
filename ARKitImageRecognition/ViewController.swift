/*
See LICENSE folder for this sample’s licensing information.

Abstract:
Main view controller for the AR experience.
*/

import ARKit
import SceneKit
import UIKit

class ViewController: UIViewController, ARSCNViewDelegate {
    
    let fadeDuration: TimeInterval = 0.3
    let rotateDuration: TimeInterval = 3
    let waitDuration: TimeInterval = 0.5
    
    @IBOutlet var sceneView: ARSCNView!
    
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    var viewObj = UIImageView(frame: CGRect(x: 0, y: 0, width: 600, height: 400))
    
    var imageHighlightAction: SCNAction {
        return .sequence([
            .wait(duration: 0.25),
            .fadeOpacity(to: 0.85, duration: 0.25),
            .fadeOpacity(to: 0.15, duration: 0.25),
            .fadeOpacity(to: 0.85, duration: 0.25),
            .fadeOut(duration: 0.5),
            .removeFromParentNode()
            ])
    }
    
    /// The view controller that displays the status and "restart experience" UI.
    lazy var statusViewController: StatusViewController = {
        return childViewControllers.lazy.compactMap({ $0 as? StatusViewController }).first!
    }()
    
    /// A serial queue for thread safety when modifying the SceneKit node graph.
    let updateQueue = DispatchQueue(label: Bundle.main.bundleIdentifier! +
        ".serialSceneKitQueue")
    
    /// Convenience accessor for the session owned by ARSCNView.
    var session: ARSession {
        return sceneView.session
    }
    
    // MARK: - View Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sceneView.delegate = self
        sceneView.session.delegate = self
        
        // Hook up status view controller callback(s).
        statusViewController.restartExperienceHandler = { [unowned self] in
            self.dismiss(animated: true, completion: nil)
//          self.restartExperience()
        }
    }

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		// Prevent the screen from being dimmed to avoid interuppting the AR experience.
		UIApplication.shared.isIdleTimerDisabled = true

        // Start the AR experience
        viewObj.image = UIImage(named: "ESI-AR-label")
        
        resetTracking()
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)

        session.pause()
	}

    // MARK: - Session management (Image detection setup)
    
    /// Prevents restarting the session while a restart is in progress.
    var isRestartAvailable = true

    /// Creates a new AR configuration to run on the `session`.
    /// - Tag: ARReferenceImage-Loading
	func resetTracking() {
        
        guard let referenceImages = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: nil) else {
            fatalError("Missing expected asset catalog resources.")
        }
        
//        let configuration = ARWorldTrackingConfiguration()
//        configuration.detectionImages = referenceImages
        
        let configuration = ARImageTrackingConfiguration()
        configuration.trackingImages = referenceImages
        configuration.maximumNumberOfTrackedImages = 1
        
        session.run(configuration, options: [.resetTracking, .removeExistingAnchors])

        statusViewController.scheduleMessage("Look around to detect images", inSeconds: 7.5, messageType: .contentPlacement)
	}

    // MARK: - ARSCNViewDelegate (Image detection results)
    /// - Tag: ARImageAnchor-Visualizing
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard let imageAnchor = anchor as? ARImageAnchor else { return }
        let referenceImage = imageAnchor.referenceImage

        DispatchQueue.main.async {
            let imageName = referenceImage.name ?? ""
            self.statusViewController.cancelAllScheduledMessages()
            self.statusViewController.showMessage("Detected image “\(imageName)”")
        }
    }
    
    //  Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
        
        guard let imageAnchor = anchor as? ARImageAnchor else { return nil }
        let referenceImage = imageAnchor.referenceImage
        let imageSize = referenceImage.physicalSize
        
        /*
        let plane = SCNPlane(width: referenceImage.physicalSize.width,
                             height: referenceImage.physicalSize.height)
        plane.firstMaterial?.diffuse.contents = UIColor.clear
        let planeNode = SCNNode(geometry: plane)
        
        planeNode.opacity = 1
        planeNode.eulerAngles.x = -.pi / 2
        
        node.addChildNode(planeNode)
        
        let childPlane = SCNPlane(width: 4.5,
                                  height: 4)

        let material = SCNMaterial()
        material.diffuse.contents = viewObj
        childPlane.materials = [material]
        let childNode = SCNNode(geometry: childPlane)
        
        childNode.opacity = 1
        childNode.position = SCNVector3Zero
        childNode.position.z = 0.15
        childNode.position.x = 3.5
        
        planeNode.addChildNode(childNode)
       */
        
        let plane = SCNPlane(width: CGFloat(imageSize.width), height: CGFloat(imageSize.height))
        plane.firstMaterial?.diffuse.contentsTransform = SCNMatrix4Translate(SCNMatrix4MakeScale(1, -1, 1), 0, 1, 0)
        
        let imageHightingAnimationNode = SCNNode(geometry: plane)
        imageHightingAnimationNode.eulerAngles.x = -.pi / 2
        imageHightingAnimationNode.opacity = 0.25
        node.addChildNode(imageHightingAnimationNode)
        
        imageHightingAnimationNode.runAction(imageHighlightAction) {
            
            let pillInfoSpriteKitScene = SKScene(fileNamed: "PillInfo")
            pillInfoSpriteKitScene?.isPaused = false

            let pillInfoPlane = SCNPlane(width: CGFloat(imageSize.width * 2.3),
                                         height: CGFloat(imageSize.height * 2))
            pillInfoPlane.firstMaterial?.diffuse.contents = pillInfoSpriteKitScene
            pillInfoPlane.firstMaterial?.diffuse.contentsTransform = SCNMatrix4Translate(
                SCNMatrix4MakeScale(1, -1, 1), 0, 1, 0)

            let pillInfoNode = SCNNode(geometry: pillInfoPlane)
            pillInfoNode.geometry?.firstMaterial?.isDoubleSided = true
            pillInfoNode.eulerAngles.x = -.pi / 2
            pillInfoNode.position = SCNVector3Zero
            node.addChildNode(pillInfoNode)
            
            let move1Action = SCNAction.move(by: SCNVector3(0.1, 0, 0), duration: 0.6)
            pillInfoNode.runAction(move1Action, completionHandler: {
//                let pillLabelNode = pillInfoSpriteKitScene?.childNode(withName: "MainNode")
//                let flipAction = SKAction.scaleX(to: -1, duration: 0.4)
//                let moveAction = SKAction.moveTo(x: 180, duration: 1.0)
//                pillLabelNode?.run(flipAction)
            })
            
        }
        
       
        
        return node
    }
}

extension SCNMaterial {
    convenience init(color: UIColor) {
        self.init()
        diffuse.contents = color
    }
    convenience init(image: UIImage) {
        self.init()
        diffuse.contents = image
    }
}
