//
//  UIButton+Theme.swift
//  ARKitImageRecognition
//
//  Created by Kevin Gaddy on 7/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit


extension UIButton {
    
    func setPrimaryEnabledStyle(){
        isEnabled = true
        setTitleColor(UIColor.white, for: .normal)
        titleLabel?.font = UIFont.preferredFont(forTextStyle: .headline)
        //layer.cornerRadius = 5
        backgroundColor = UIColor(rgb: 0x009bdf)
    }
    
    func setSecondaryEnabledStyle(){
        isEnabled = true
        setTitleColor(UIColor.white, for: .normal)
        titleLabel?.font = UIFont.preferredFont(forTextStyle: .headline)
        // layer.cornerRadius = 5
        backgroundColor = UIColor.red
    }
    
    func setDisabledStyle(){
        isEnabled = false
        alpha = 0.25
    }
}
