//
//  HomeViewController.swift
//  ARKitImageRecognition
//
//  Created by Kevin Gaddy on 7/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBAction func arAction(_ sender: Any) {
        let board = UIStoryboard(name: "Main", bundle: nil)
        if let vc = board.instantiateInitialViewController() as? ViewController  {
            //let navigation = UINavigationController(rootViewController: vc)
           // navigation.navigationBar.barTintColor = UIColor(rgb: 0x009bdf)
            //UIColor(rgb: 0xf8f8f9)
           // navigation.navigationBar.tintColor = UIColor.white
           // navigation.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
            self.present(vc, animated: true, completion: nil)
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor =  UIColor(rgb: 0xf8f8f9)
        self.navigationController?.navigationBar.topItem?.title = "Home"
     
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
