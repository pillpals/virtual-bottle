//
//  LoginViewController.swift
//  ARKitImageRecognition
//
//  Created by Kevin Gaddy on 7/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBAction func loginAction(_ sender: Any) {
   
        let board = UIStoryboard(name: "home", bundle: nil)
        if let vc = board.instantiateInitialViewController() as? HomeViewController  {
            let navigation = UINavigationController(rootViewController: vc)
            navigation.navigationBar.barTintColor = UIColor(rgb: 0x009bdf)
            navigation.navigationBar.tintColor = UIColor.white
            navigation.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
            self.present(navigation, animated: true, completion: nil)
        }
        
      
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor =  UIColor(rgb: 0xf2fafd)
        loginButton.setPrimaryEnabledStyle()
        titleLabel.textColor = UIColor(rgb: 0x009bdf)
        subTitleLabel.textColor = UIColor(rgb: 0x777777)
    }
  

}
